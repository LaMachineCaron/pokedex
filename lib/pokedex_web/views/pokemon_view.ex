defmodule PokedexWeb.PokemonView do
  use PokedexWeb, :view
  alias PokedexWeb.PokemonView

  def render("index.json", %{paginated_pokemons: paginated_pokemons}) do
    %{data: render_many(paginated_pokemons.entries, PokemonView, "pokemon.json"),
      pagination: %{next_token: paginated_pokemons.metadata.after,
                    max_result: paginated_pokemons.metadata.limit,
                    total_count: paginated_pokemons.metadata.total_count}}
  end

  def render("show.json", %{pokemon: pokemon}) do
    %{data: render_one(pokemon, PokemonView, "pokemon.json")}
  end

  def render("pokemon.json", %{pokemon: pokemon}) do
    %{id: pokemon.id,
      number: pokemon.number,
      name: pokemon.name,
      primary_type: pokemon.primary_type,
      secondary_type: pokemon.secondary_type,
      total: pokemon.total,
      hp: pokemon.hp,
      attack: pokemon.attack,
      defense: pokemon.defense,
      special_attack: pokemon.special_attack,
      special_defense: pokemon.special_defense,
      speed: pokemon.speed,
      generation: pokemon.generation,
      legendary: pokemon.legendary}
  end
end
