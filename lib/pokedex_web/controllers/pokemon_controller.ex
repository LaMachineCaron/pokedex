defmodule PokedexWeb.PokemonController do
  use PokedexWeb, :controller

  alias Pokedex.Data
  alias Pokedex.Data.Pokemon

  action_fallback PokedexWeb.FallbackController

  def index(conn, params) do
    token = Map.get(params, "pagination_token", nil)
    case params |> Map.get("max_result", "10") |> Integer.parse() do
      {max_result, _} ->
        paginated_pokemons = Data.paginate_pokemons(after: token, limit: max_result)
        render(conn, "index.json", paginated_pokemons: paginated_pokemons)
      :error ->
        {:error, :bad_request}
    end

  end

  def create(conn, %{"pokemon" => pokemon_params}) do
    with {:ok, %Pokemon{} = pokemon} <- Data.create_pokemon(pokemon_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.pokemon_path(conn, :show, pokemon))
      |> render("show.json", pokemon: pokemon)
    end
  end

  def show(conn, %{"id" => id}) do
    pokemon = Data.get_pokemon!(id)
    render(conn, "show.json", pokemon: pokemon)
  end

  def update(conn, %{"id" => id, "pokemon" => pokemon_params}) do
    pokemon = Data.get_pokemon!(id)

    with {:ok, %Pokemon{} = pokemon} <- Data.update_pokemon(pokemon, pokemon_params) do
      render(conn, "show.json", pokemon: pokemon)
    end
  end

  def delete(conn, %{"id" => id}) do
    pokemon = Data.get_pokemon!(id)

    with {:ok, %Pokemon{}} <- Data.delete_pokemon(pokemon) do
      send_resp(conn, :no_content, "")
    end
  end
end
