defmodule Pokedex.Release do
  @app :pokedex
  require Logger

  def migrate_and_seed do
    load_app()

    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
      {:ok, _, _} = Ecto.Migrator.with_repo(Pokedex.Repo, fn repo ->
        if Pokedex.Data.paginate_pokemons(limit: 1).entries |> Enum.empty?  do
          seed_file = "#{:code.priv_dir(:pokedex)}/repo/seeds.exs"
          Logger.info "Seeding from #{seed_file}"
          Code.require_file(seed_file)
        end
      end)
    end
  end

  def rollback(repo, version) do
    load_app()
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp load_app do
    Application.load(@app)
  end
end
