defmodule Pokedex.Data.Pokemon do
  use Ecto.Schema
  import Ecto.Changeset

  schema "pokemons" do
    field :attack, :integer
    field :defense, :integer
    field :generation, :integer
    field :hp, :integer
    field :legendary, :boolean, default: false
    field :name, :string
    field :number, :integer
    field :primary_type, :string
    field :secondary_type, :string
    field :special_attack, :integer
    field :special_defense, :integer
    field :speed, :integer
    field :total, :integer

    timestamps()
  end

  @doc false
  def changeset(pokemon, attrs) do
    pokemon
    |> cast(attrs, [:number, :name, :primary_type, :secondary_type, :total, :hp, :attack, :defense, :special_attack, :special_defense, :speed, :generation, :legendary])
    |> validate_required([:number, :name, :primary_type, :total, :hp, :attack, :defense, :special_attack, :special_defense, :speed, :generation, :legendary])
  end
end
