defmodule Pokedex.Repo do
  use Ecto.Repo,
    otp_app: :pokedex,
    adapter: Ecto.Adapters.MyXQL

  use Paginator,
    maximum_limit: 100,
    include_total_count: true
end
