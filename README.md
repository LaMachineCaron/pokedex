# Pokedex

## Getting started
Copy `.env.sample` to `.env`. For simplicity, `.env.sample` comes with values out of the box. 
```bash
cp .env.sample .env
```

To start the infra (server and database):
```bash
docker-compose build && docker-compose up -d
```

## API

### Index
Returns a paginated list of pokemon. To request the next page, use the field `pagination.next_token` from the previous response.

It is also possible to change the number of result per page using the variable `max_result`. Note that it's value is capped at 100.

#### Examples
`GET http://localhost:4000/api/pokemon`

`GET http://localhost:4000/api/pokemons?pagination_token=g3QAAAABZAACaWRhCw==`

`GET http://localhost:4000/api/pokemons?max_result=50`

### Show
Returns the requested pokemon using their `id`.

#### Examples
`GET http://localhost:4000/api/pokemon/42`

### Create
Creates a pokemon using the body's data. Fields `secondary_type` and `legendary` are optionals, they default to `null` and `false` respectively.

#### Examples
```
POST http://localhost:4000/api/pokemons
{
    "pokemon": {
        "number": 42,
        "name": "Alex",
        "attack": 42,
        "defense": 42,
        "generation": 1995,
        "hp": 9000,
        "primary_type": "Human",
        "secondary_type": "dev"
        "special_attack": 42,
        "specical_defense": 42,
        "speed": 42,
        "total": 42,
        "legendary": false
    }   
}
```

### Update
Updates a pokemon using the body's data. Only the fields to update are required.

#### Examples
```
PUT http://localhost:4000/api/pokemons/42
{
    "pokemon": {
        "name": "Something else"
    }
}
```

### Destroy
Deletes the pokemon specified by the id.

#### Examples
`DELETE http://localhost:4000/api/pokemons/42`
