defmodule Pokedex.DataTest do
  use Pokedex.DataCase

  alias Pokedex.Data

  describe "pokemons" do
    alias Pokedex.Data.Pokemon

    @valid_attrs %{attack: 42, defense: 42, generation: 42, hp: 42, legendary: true, name: "some name", number: 42, primary_type: "some primary_type", secondary_type: "some secondary_type", special_attack: 42, special_defense: 42, speed: 42, total: 42}
    @update_attrs %{attack: 43, defense: 43, generation: 43, hp: 43, legendary: false, name: "some updated name", number: 43, primary_type: "some updated primary_type", secondary_type: "some updated secondary_type", special_attack: 43, special_defense: 43, speed: 43, total: 43}
    @invalid_attrs %{attack: nil, defense: nil, generation: nil, hp: nil, legendary: nil, name: nil, number: nil, primary_type: nil, secondary_type: nil, special_attack: nil, special_defense: nil, speed: nil, total: nil}

    def pokemon_fixture(attrs \\ %{}) do
      {:ok, pokemon} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Data.create_pokemon()

      pokemon
    end

    test "list_pokemons/0 returns all pokemons" do
      pokemon = pokemon_fixture()
      assert Data.list_pokemons() == [pokemon]
    end

    test "get_pokemon!/1 returns the pokemon with given id" do
      pokemon = pokemon_fixture()
      assert Data.get_pokemon!(pokemon.id) == pokemon
    end

    test "create_pokemon/1 with valid data creates a pokemon" do
      assert {:ok, %Pokemon{} = pokemon} = Data.create_pokemon(@valid_attrs)
      assert pokemon.attack == 42
      assert pokemon.defense == 42
      assert pokemon.generation == 42
      assert pokemon.hp == 42
      assert pokemon.legendary == true
      assert pokemon.name == "some name"
      assert pokemon.number == 42
      assert pokemon.primary_type == "some primary_type"
      assert pokemon.secondary_type == "some secondary_type"
      assert pokemon.special_attack == 42
      assert pokemon.special_defense == 42
      assert pokemon.speed == 42
      assert pokemon.total == 42
    end

    test "create_pokemon/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Data.create_pokemon(@invalid_attrs)
    end

    test "update_pokemon/2 with valid data updates the pokemon" do
      pokemon = pokemon_fixture()
      assert {:ok, %Pokemon{} = pokemon} = Data.update_pokemon(pokemon, @update_attrs)
      assert pokemon.attack == 43
      assert pokemon.defense == 43
      assert pokemon.generation == 43
      assert pokemon.hp == 43
      assert pokemon.legendary == false
      assert pokemon.name == "some updated name"
      assert pokemon.number == 43
      assert pokemon.primary_type == "some updated primary_type"
      assert pokemon.secondary_type == "some updated secondary_type"
      assert pokemon.special_attack == 43
      assert pokemon.special_defense == 43
      assert pokemon.speed == 43
      assert pokemon.total == 43
    end

    test "update_pokemon/2 with invalid data returns error changeset" do
      pokemon = pokemon_fixture()
      assert {:error, %Ecto.Changeset{}} = Data.update_pokemon(pokemon, @invalid_attrs)
      assert pokemon == Data.get_pokemon!(pokemon.id)
    end

    test "delete_pokemon/1 deletes the pokemon" do
      pokemon = pokemon_fixture()
      assert {:ok, %Pokemon{}} = Data.delete_pokemon(pokemon)
      assert_raise Ecto.NoResultsError, fn -> Data.get_pokemon!(pokemon.id) end
    end

    test "change_pokemon/1 returns a pokemon changeset" do
      pokemon = pokemon_fixture()
      assert %Ecto.Changeset{} = Data.change_pokemon(pokemon)
    end
  end
end
