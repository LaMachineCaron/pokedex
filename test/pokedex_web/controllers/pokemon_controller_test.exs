defmodule PokedexWeb.PokemonControllerTest do
  use PokedexWeb.ConnCase

  alias Pokedex.Data
  alias Pokedex.Data.Pokemon

  @create_attrs %{
    attack: 42,
    defense: 42,
    generation: 42,
    hp: 42,
    legendary: true,
    name: "some name",
    number: 42,
    primary_type: "some primary_type",
    secondary_type: "some secondary_type",
    special_attack: 42,
    special_defense: 42,
    speed: 42,
    total: 42
  }
  @update_attrs %{
    attack: 43,
    defense: 43,
    generation: 43,
    hp: 43,
    legendary: false,
    name: "some updated name",
    number: 43,
    primary_type: "some updated primary_type",
    secondary_type: "some updated secondary_type",
    special_attack: 43,
    special_defense: 43,
    speed: 43,
    total: 43
  }
  @invalid_attrs %{attack: nil, defense: nil, generation: nil, hp: nil, legendary: nil, name: nil, number: nil, primary_type: nil, secondary_type: nil, special_attack: nil, special_defense: nil, speed: nil, total: nil}

  def fixture(:pokemon) do
    {:ok, pokemon} = Data.create_pokemon(@create_attrs)
    pokemon
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all pokemons", %{conn: conn} do
      conn = get(conn, Routes.pokemon_path(conn, :index))
      assert json_response(conn, 200) == %{
        "data" => [],
        "pagination" => %{
          "total_count" => 0,
          "max_result" => 10,
          "next_token" => nil
        }
      }
    end

    test "lists a variable amount of pokemons", %{conn: conn} do
      conn = get(conn, Routes.pokemon_path(conn, :index, %{max_result: 42}))
      assert json_response(conn, 200)["pagination"]["max_result"] == 42
    end

    test "lists a maximum of 100 pokemons", %{conn: conn} do
      conn = get(conn, Routes.pokemon_path(conn, :index, %{max_result: 9000}))
      assert json_response(conn, 200)["pagination"]["max_result"] == 100
    end

    test "lists a second page of pokemon", %{conn: conn} do
      create_pokemon(:first)
      create_pokemon(:second)

      conn = get(conn, Routes.pokemon_path(conn, :index, %{max_result: 1}))
      next_token = json_response(conn, 200)["pagination"]["next_token"]
      first_page = json_response(conn, 200)["data"]

      conn = get(conn, Routes.pokemon_path(conn, :index, %{max_result: 1, pagination_token: next_token}))
      assert json_response(conn, 200)["data"] != first_page
    end
  end

  describe "create pokemon" do
    test "renders pokemon when data is valid", %{conn: conn} do
      conn = post(conn, Routes.pokemon_path(conn, :create), pokemon: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.pokemon_path(conn, :show, id))

      assert %{
               "id" => _id,
               "attack" => 42,
               "defense" => 42,
               "generation" => 42,
               "hp" => 42,
               "legendary" => true,
               "name" => "some name",
               "number" => 42,
               "primary_type" => "some primary_type",
               "secondary_type" => "some secondary_type",
               "special_attack" => 42,
               "special_defense" => 42,
               "speed" => 42,
               "total" => 42
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.pokemon_path(conn, :create), pokemon: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update pokemon" do
    setup [:create_pokemon]

    test "renders pokemon when data is valid", %{conn: conn, pokemon: %Pokemon{id: id} = pokemon} do
      conn = put(conn, Routes.pokemon_path(conn, :update, pokemon), pokemon: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.pokemon_path(conn, :show, id))

      assert %{
               "id" => _id,
               "attack" => 43,
               "defense" => 43,
               "generation" => 43,
               "hp" => 43,
               "legendary" => false,
               "name" => "some updated name",
               "number" => 43,
               "primary_type" => "some updated primary_type",
               "secondary_type" => "some updated secondary_type",
               "special_attack" => 43,
               "special_defense" => 43,
               "speed" => 43,
               "total" => 43
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, pokemon: pokemon} do
      conn = put(conn, Routes.pokemon_path(conn, :update, pokemon), pokemon: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete pokemon" do
    setup [:create_pokemon]

    test "deletes chosen pokemon", %{conn: conn, pokemon: pokemon} do
      conn = delete(conn, Routes.pokemon_path(conn, :delete, pokemon))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.pokemon_path(conn, :show, pokemon))
      end
    end
  end

  defp create_pokemon(_) do
    pokemon = fixture(:pokemon)
    %{pokemon: pokemon}
  end
end
