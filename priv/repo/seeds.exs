# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Pokedex.Repo.insert!(%Pokedex.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

pokemon_attributes = [
  :number,
  :name,
  :primary_type,
  :secondary_type,
  :total,
  :hp,
  :attack,
  :defense,
  :special_attack,
  :special_defense,
  :speed,
  :generation,
  :legendary,
]

parse_boolean = fn
 "True" -> true
 "False" -> false
end

File.stream!("#{:code.priv_dir(:pokedex)}/repo/pokemons.csv")
  |> Stream.drop(1) # Skip the header line
  |> Stream.map(&String.trim/1)
  |> Stream.map(&String.split(&1, ","))
  |> Stream.map(&Enum.zip(pokemon_attributes, &1))
  |> Stream.map(&Enum.into(&1, %{}))
  |> Stream.map(&Map.update!(&1, :legendary, parse_boolean))
  |> Stream.map(&Pokedex.Data.create_pokemon/1)
  |> Stream.run()