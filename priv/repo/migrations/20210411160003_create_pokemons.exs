defmodule Pokedex.Repo.Migrations.CreatePokemons do
  use Ecto.Migration

  def change do
    create table(:pokemons) do
      add :number, :integer
      add :name, :string
      add :primary_type, :string
      add :secondary_type, :string
      add :total, :integer
      add :hp, :integer
      add :attack, :integer
      add :defense, :integer
      add :special_attack, :integer
      add :special_defense, :integer
      add :speed, :integer
      add :generation, :integer
      add :legendary, :boolean, default: false, null: false

      timestamps()
    end

  end
end
