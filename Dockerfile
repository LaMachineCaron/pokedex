FROM elixir:1.11-alpine AS build

# set build ENV
ENV MIX_ENV=prod

# install hex + rebar
RUN mix local.hex --force && mix local.rebar --force

WORKDIR /app

# install mix dependencies
COPY mix.exs mix.lock ./
COPY config config
RUN mix deps.get --ony-prod && mix compile

# build assets
COPY priv priv
RUN mix phx.digest

# compile
COPY lib lib
RUN mix release

FROM elixir:1.11-alpine AS app

WORKDIR /app

RUN chown nobody:nobody /app

USER nobody:nobody

COPY --from=build --chown=nobody:nobody /app/_build .

ENV HOME=/app

CMD ["./prod/rel/pokedex/bin/pokedex", "start"]